#
# Usage:
#  cd /etc/puppet
#  sudo git pull origin master && sudo puppet apply manifests/vm-ubuntu64.pp
#

# PATH
Exec {
  path => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
}

# Nätverkskort och IP Tables
# ===================================
file { '/etc/network/interfaces':
    ensure => present,
    owner => 'root',
    group => 'root',
    source => '/etc/puppet/files/vm-ubuntu64/etc/network/interfaces',
}

# Puppet master ... via git
# ===================================
file { '/usr/local/bin/git-pull-puppet-apply.sh':
    ensure => present,
    mode => '0755',
    owner => 'root',
    group => 'root',
    source => '/etc/puppet/files/vm-ubuntu64/usr/local/bin/git-pull-puppet-apply.sh',
}

cron { 'git-pull-puppet-apply-manifest':
    ensure  => 'present',
    command => '/usr/local/bin/git-pull-puppet-apply.sh',
    user => 'root', 
    hour => '*/7', 
    minute => '7', 
    require => File['/usr/local/bin/git-pull-puppet-apply.sh'],
}

# Firewall - IPTABLES
##
include the_iptables
include moped::firewall

class the_iptables {
    resources { "firewall":
        purge => true
    }

    Firewall {
        before  => Class['the_iptables::post'],
        require => Class['the_iptables::pre'],
    }
    
    class { ['the_iptables::pre', 'the_iptables::post']: }
    class { 'firewall': }
}

class the_iptables::pre {
  Firewall {
    require => undef,
  }
 
  # Default firewall rules
  firewall { '000 accept all icmp':
    proto   => 'icmp',
    action  => 'accept',
  }->
  firewall { '001 accept all to lo interface':
    proto   => 'all',
    iniface => 'lo',
    action  => 'accept',
  }->
  firewall { '002 accept related established rules':
    proto   => 'all',
    state   => ['RELATED', 'ESTABLISHED'],
    action  => 'accept',
  }->
  firewall { '003 allow SSH access':
    dport   => 22,
    proto  => tcp,
    action => accept,
  }
}

class the_iptables::post {
  firewall { '999 drop all':
    proto   => 'all',
    action  => 'drop',
    before  => undef,
  }
}

class moped::firewall {
    firewall { '99 allow moped web interface':
        dport   => 6680,
        proto  => tcp,
        action => accept,
    }
}